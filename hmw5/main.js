// Теоретический вопрос
// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Экранирование - это использование косой черты для того, чтобы из специального символа сделать обычный. например
// символ "" воспринимается в JavaScript как строка, и для того, чтобы записать строку в кавычках, нужно использовать либо кавычки другого типа,
// либо экранировать их, тогда они будут восприниматься как обычные символы и проблем не возникнет. Тоже самое справедливо для символов .?$[]()*+\|

// Задача
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function createNewUser (){
    let newUser = {
         userName: prompt('Введите ваше имя'),
         userSurname: prompt('Введите вашу фамилию'),
         userBirth: prompt('Введите дату рождения в формате dd.mm.yyyy'),
         getLogin: function (name, surname) {
             let NameFirstLetter = name.substr(0, 1);
             let userLogin = `${NameFirstLetter}${surname}`;
             console.log(userLogin.toLowerCase());
         },
         getAge: function (userBirth) {
   
            let updatedBirth = Number(userBirth);
            let birthday = new Date(userBirth);
            let now = new Date();
            let milliDay = 1000 * 60 * 60 * 24;
            let ageInDays = (now - birthday) / milliDay;
            let ageInYears = Math.floor(ageInDays / 365);
            console.log (ageInYears);
         },
         getPassword: function (name, surname, birthday){
            let userNameFirstLetter = name.substr(0, 1);
            let userBirthYear = birthday.substr(6, 4);
            let firstLetterToUpper = userNameFirstLetter.toUpperCase();
            let userSurnameToLower = surname.toLowerCase();
            let userPassword = `${firstLetterToUpper}${userSurnameToLower}${userBirthYear}`;
            console.log(userPassword);
         },
     }
     newUser.getAge(newUser.userBirth);
     newUser.getPassword(newUser.userName, newUser.userSurname, newUser.userBirth);
     return newUser.getLogin(newUser.userName, newUser.userSurname);
     
 };
 createNewUser();
 

    
  




 


