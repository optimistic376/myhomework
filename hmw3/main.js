
// Описать своими словами для чего вообще нужны функции в программировании.
//  для того чтобы однотипную логику не нужно было многократно писать, а можно было использовать из любого места в коде.
// Описать своими словами, зачем в функцию передавать аргумент.
// Чтобы при вызове функции присвоить параметрам соответствующие значения(аргуметы) и произвести указанную в функции операцию.
                                   
//                                                  Задача
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. 
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// Считать с помощью модального окна браузера два числа.
// Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
// Вывести в консоль результат выполнения функции.


// Необязательное задание продвинутой сложности:

// После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, 
// - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).




(function() {
    const values = {
      firstNumber: null,
      secondNumber: null,
      operand: null
    }

    function setValues() {
      values.firstNumber = prompt('Print first number');
      values.secondNumber = prompt('Print second number');
    }

    function checkValue(val) {
      if(!val) {
          alert('All values should be appointed');
        return setValues();
      } 
      if(isNaN(val)) {
        alert('Only numbers alowed');
        return setValues();
      }
    }

    function validate() {
      checkValue(values.firstNumber);
      checkValue(values.secondNumber);
    }

    function isCalculator (a, b, c){ 
   
        switch(c){
            case '+':
                console.log (a + b);
                break;
            case '-':
                console.log (a - b); 
                break;
            case '/':
                console.log(a / b);
                break;
            case '*':
                console.log(a * b);
                break;        
        };
       
        }
  
    setValues();
    validate();
    values.operand = prompt('What to do with numbers?');
    isCalculator(values.firstNumber, values.secondNumber, values.operand);
    
  })();




 


