// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// У обектов в JS есть скрытое свойство prototype которое может ссылатся на другой обьект. Таким образом,
// мы можем при создании нового обекта брать за основу уже созданный и дополнять его необходимыми свойствами. Также, если у нас есть 
// ряд повторяющихся свойств можно один раз записать их в prototype и использовать для каждого обекта.
class Emploee {
    constructor (name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    getName() {
        return this.name;
      }
    getAge() {
        return this.age;
      }
    getSalary() {
        return this.salary;
      }
}

class Programmer extends Emploee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }

    getSalary() {
        return this.salary * 3;
      }
}

const programmer1 = new Programmer ('Julia', 23, 30000, 'JavaScript');
const programmer2 = new Programmer ('Alina', 20, 35000, 'Python');
const programmer3 = new Programmer ('Arkadii', 21, 37000, 'C++');

console.log(programmer1, programmer2, programmer3);

console.log(programmer1.getSalary());