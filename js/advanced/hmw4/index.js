const BASE_URL = 'https://ajax.test-danit.com/api/swapi/films';
const container = document.querySelector(".container");

async function fetchFilmsData() { 
  const response = await fetch(BASE_URL);
  const data = await response.json();
  return data;
}



function renderInfoFilm(a, episodeId, name, openingCrawl) { 
  const html = ` <div class="film-card">
    <h3>Episode id:${episodeId}</h3> 
    <h3>Episode name:${name}</h3> 
    <p>${openingCrawl}</p>
    <ol class="char_list_${a}"></ol>
</div>`;
  container.insertAdjacentHTML("beforeend", html);
};


async function getCharacters(a, characters) {
    for (let i = 0; i < characters.length; i++) {
      const charUrl = characters[i];
      const response = await fetch(charUrl);
      const charInfo = await response.json();
      const data = charInfo.name;
      let characterBlock = document.querySelector(`.char_list_${a}`)
      const html = `<li class="char_item">${data}</li>`;
      characterBlock.insertAdjacentHTML("beforeend", html);
    }
  }


document.addEventListener("DOMContentLoaded", async () => {
  const films = await fetchFilmsData();
  for (let a = 0; a < films.length; a++) {
    let {
      episodeId,
      name,
      openingCrawl,
      characters,
    } = films[a];
    console.log(films[a])
    renderInfoFilm(a, episodeId, name, openingCrawl);
    getCharacters(a, characters);
  }
});