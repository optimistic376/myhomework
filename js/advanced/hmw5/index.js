/*
Создать простую HTML страницу с кнопкой Вычислить по IP.
По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, 
получить оттуда IP адрес клиента.
Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о 
физическом адресе.
Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, 
страна, регион, город, район города.
Все запросы на сервер необходимо выполнить с помощью async await.
*/
const button = document.querySelector(".button");
const container = document.querySelector(".container");

async function clickHandler(event) {

    const {ip} = await getUserIP();
    const adress = await getUserAdress(ip);
    
    if ((adress.status = "success")) {
        const {continent, country, regionName, city, district} = adress;
        container.innerHTML = "";
        container.insertAdjacentHTML(
            "beforeend",
            `<p>You adress is: ${continent}, ${country}, ${regionName}, ${city}, ${district}</p> `
        )
    } else {
        console.error(ip.message);
    }
};


function getUserIP() {
    const baseUrl = "https://api.ipify.org/?format=json";
    return fetch(baseUrl)
        .then((response) => response.json());
};


function getUserAdress(ip) {
    const baseUrl = "http://ip-api.com/json/"
    return fetch(
        baseUrl + ip + "?fields=status,message,continent,country,regionName,city,district")
        .then((response) => response.json());
};

button.addEventListener("click", clickHandler);