// Теоретический вопрос

// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

// DOM это дерево из HTML тэгов, которые представлены ввиде обьектов, при этом 
// вложенные тэги являются детьми родительского элемента. DOM отображает структуру html документа, а также,
// предоставляет возможность добавить или убрать какой-то элемент не изменяя html документ.


// Задание
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

// Технические требования:

// Создать функцию, которая будет принимать на вход массив 
// и опциональный второй аргумент parent - DOM-элемент, 
// к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования 
// контента списка перед выведением его на страницу;

// Примеры массивов, которые можно выводить на экран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];



 let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


function concludeDomElement (array, element){
  let fragment = document.createDocumentFragment(),

    i, li, 

    ul = document.createElement('ul');

  array.map(function(item) {
    li = document.createElement('li');
    li.textContent = item;
    fragment.appendChild(li);
});

ul.appendChild(fragment);
element.appendChild(ul);

};
concludeDomElement (arr, document.body);

